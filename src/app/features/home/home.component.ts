import { Component, OnInit } from '@angular/core';
import { EmployersService } from 'src/app/services/employers.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
})
export class HomeComponent implements OnInit {
  employeeCollection: any;
  selectedEmployee: string;

  constructor(private employerService: EmployersService) {}

  ngOnInit(): void {
    this.getEmployeeList();
  }
  getSelectedValue(color: string): void {
    this.selectedEmployee = color;
  }
  getEmployeeList(): void {
    this.employerService.getEmployers().subscribe((res) => {
      this.employeeCollection = res.data;
    });
  }
}
