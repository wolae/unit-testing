import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DropdownlistComponent } from './shared/components/dropdownlist/dropdownlist.component';
import { HomeComponent } from './features/home/home.component';
import { CapitalizePipe } from './shared/pipes/capitalize.pipe';
import { ChangeColorDirective } from './shared/directives/change-color.directive';

@NgModule({
  declarations: [AppComponent, DropdownlistComponent, HomeComponent, CapitalizePipe, ChangeColorDirective],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
