import { CapitalizePipe } from './capitalize.pipe';

describe('CapitalizePipe', () => {
  // Instanciamos la clase CapitalizePipe
  const pipe = new CapitalizePipe();

  // Comprobamos que nuestra instancia esta creada
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  // Comprobamos que nuestra pipe funciona con textos vacios
  it('should transform "" to ""', () => {
    expect(pipe.transform('')).toBe('');
  });

  it('should transform "tiger" to "Tiger"', () => {
    expect(pipe.transform('tiger')).toBe('Tiger');
  });

  it('should transform "tiger nixon" to "Tiger Nixon"', () => {
    expect(pipe.transform('tiger nixon')).toBe('Tiger Nixon');
  });
});
