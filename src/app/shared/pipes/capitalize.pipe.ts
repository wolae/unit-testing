import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize',
})
export class CapitalizePipe implements PipeTransform {
  transform(input: string): string {
    if (input !== undefined) {
      return input.length === 0
        ? ''
        : input.replace(
            /\w\S*/g,
            (txt) => txt[0].toUpperCase() + txt.substr(1).toLowerCase()
          );
    }
  }
}
