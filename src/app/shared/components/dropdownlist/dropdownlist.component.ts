import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-dropdownlist',
  templateUrl: './dropdownlist.component.html',
  styleUrls: ['./dropdownlist.component.sass'],
})
export class DropdownlistComponent implements OnInit {
  @Input() itemArray: any[];
  @Output() itemSelected: EventEmitter<string> = new EventEmitter<string>();
  constructor() {}

  ngOnInit(): void {}
  onChange(item: string): void {
    this.itemSelected.emit(item);
  }
}
