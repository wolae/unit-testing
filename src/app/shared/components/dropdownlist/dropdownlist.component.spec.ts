import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownlistComponent } from './dropdownlist.component';

describe('DropdownlistComponent', () => {
  let component: DropdownlistComponent;
  let fixture: ComponentFixture<DropdownlistComponent>;
  const myStringArray = ['Emloyeee 1', 'Employee 2', 'Employee 3'];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DropdownlistComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownlistComponent);
    component = fixture.componentInstance;
  });
  // Comprobamos que nuestro componente esta instanciado
  it('should create', () => {
    expect(component).toBeDefined();
  });

  // Comprobamos que nuestro componente tiene un elemento HTML select
  it('should have a select dropdown', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('select')).toBeDefined();
  });

  // Comprobamos que nuestro input recibe array de string y genera las opciones en el desplegable
  it('should receive a string array input and generate drop down list options', () => {
    component.itemArray = myStringArray;
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    const options = compiled.querySelectorAll('option').length;
    expect(options).toBeGreaterThanOrEqual(3);
  });

  // Comprobamos que nuestro output emite la opcion seleccionada.
  it('Should return a string when we select a option', () => {
    component.itemArray = myStringArray;
    // Nos ponemos a la escucha del método "emit" de nuestro output.
    spyOn(component.itemSelected, 'emit');
    // Detectamos cambios
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    const select: HTMLSelectElement = compiled.querySelector('select');
    // seleccionamos un nuevo valor
    select.value = select.options[3].value;
    const selectedValue = select.value;
    // Lanzamos el evento change de nuestro select
    select.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    // Comprobamos con la funcion toHaveBeenCalledWith que nuestro EventEmitter emite nuestro valor seleccionado.
    expect(component.itemSelected.emit).toHaveBeenCalledWith(selectedValue);
  });
});
