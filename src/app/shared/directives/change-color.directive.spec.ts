import { Component, DebugElement, ElementRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ChangeColorDirective } from './change-color.directive';
@Component({
  template: ` <h2 appChangeColor>Something Red</h2>
    <h2>No Red color</h2>`,
})
class TestComponent {}

describe('ChangeColorDirective', () => {
  let directiveElements: DebugElement[];

  beforeEach(() => {
    const fixture = TestBed.configureTestingModule({
      declarations: [ChangeColorDirective, TestComponent],
    }).createComponent(TestComponent);

    fixture.detectChanges();

    // Recogemos todos los elementos con la directiva appChangeColor
    directiveElements = fixture.debugElement.queryAll(
      By.directive(ChangeColorDirective)
    );
  });

  it('should text color be "red"', () => {
    const textColor = directiveElements[0].nativeElement.style.color;
    expect(textColor).toBe('red');
  });
});
