import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { EmployersService } from './employers.service';

describe('EmployersService', () => {
  let employerService: EmployersService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EmployersService],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    employerService = TestBed.inject(EmployersService);
  });

  // Despues de cada test nos aseguramos de que no hay peticiones pendientes.
  afterEach(() => {
    httpTestingController.verify();
  });

  // Comprobamos que nuestro servicio esta instanciado
  it('Should be instanciated', () => {
    expect(employerService).toBeDefined();
  });

  // Comprobamos que nuestra respuesta es satisfactoria
  it('Should get a successful response ', () => {
    employerService
      .getEmployers()
      .subscribe((res) => expect(res.status).toContain('success'), fail);

    // Comprobamos que nuestra peticion se ha realizado correctamente a nuestro endpoint
    const req = httpTestingController.expectOne(
      'http://dummy.restapiexample.com/api/v1/employees'
    );
    expect(req.request.method).toEqual('GET');
  });
});
