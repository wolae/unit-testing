import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class EmployersService {
  constructor(private http: HttpClient) {}

  public getEmployers(): Observable<any> {
    return this.http.get('http://dummy.restapiexample.com/api/v1/employees');
  }
}
